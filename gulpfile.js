// define packages
var gulp =              require('gulp');
var autoprefixer =      require('gulp-autoprefixer');
var gutil =             require( 'gulp-util' );
var ftp =               require('vinyl-ftp');

// define remote path from root to install folder
var remoteRoot =        'public_html/hosted/lcss2016/viz/';

// define ftp data
var ftpData =  
{
    host:                 'lcss2016.ruggerocastagnola.com',
    user:                 'ruggeroc',
    password:             'JOs”39s)3_W4xWSf',
    port:                 21,
    connect:              connect
}; 

// define files to upload
var filesToUpload = 
[
    'css/*.css',
    'js/*.js',
    'js/*.map',
    'assets/*.json',
    'index.html'
];

gulp.task('autodeploy', function(){ 
    var conn = connect(ftpData);
    gulp.watch(filesToUpload).on('change', function(file){

        if(file.path.indexOf('.css') > -1) {
            return gulp.src(file.path)
                    .pipe(autoprefixer('last 40 versions'))
                    .pipe(conn.dest(remoteRoot + folder(file)));
        } else {
            return gulp.src(file.path)
                    .pipe(conn.dest(remoteRoot + folder(file)));
        }
    });
});

var folder = function(file){
    if(file.path.indexOf('.json') > -1) {
        return 'assets/';
    } else
    if(file.path.indexOf('.js') > -1 || file.path.indexOf('.map') > -1) {
        return 'js/';
    } else
    if(file.path.indexOf('.css') > -1) {
        return 'css/';
    }
    return '';
};

// utils
var connect = function(ftpData){
    return ftp.create({
        host:       ftpData.host,
        port:       ftpData.port,
        user:       ftpData.user,
        password:   ftpData.password,
        parallel:   1,
        log:        gutil.log
    });
};